$(document).ready(function () {
	    	// Graph scripts here
	    	var graphData = [{
	        		// Employee1 data [week number, number of minutes worked]
				        data: [ [1, 1300], [2, 1600], [3, 1900], [4, 2100], [5, 2500], [6, 2200], [7, 2000], [8, 1950], [9, 1900], [10, 2000] ],
				        color: '#71c73e'
				    }, {
				        // Employee2 data [week number, number of minutes worked]
				        data: [ [1, 500], [2, 600], [3, 550], [4, 600], [5, 800], [6, 900], [7, 800], [8, 850], [9, 830], [10, 1000] ],
				        color: '#77b7c5',
				        points: { radius: 4, fillColor: '#77b7c5' }
				    }
				];

			// Lines
			$.plot($('#graph-lines'), graphData, {
			    series: {
			        points: {
			            show: true,
			            radius: 5
			        },
			        lines: {
			            show: true
			        },
			        shadowSize: 0
			    },
			    grid: {
			        color: '#646464',
			        borderColor: 'transparent',
			        borderWidth: 20,
			        hoverable: true
			    },
			    xaxis: {
			        tickColor: 'transparent',
			        tickDecimals: 2
			    },
			    yaxis: {
			        tickSize: 500
			    }
			});
			 
			// Bars
			$.plot($('#graph-bars'), graphData, {
			    series: {
			        bars: {
			            show: true,
			            barWidth: .9,
			            align: 'center'
			        },
			        shadowSize: 0
			    },
			    grid: {
			        color: '#646464',
			        borderColor: 'transparent',
			        borderWidth: 20,
			        hoverable: true
			    },
			    xaxis: {
			        tickColor: 'transparent',
			        tickDecimals: 2
			    },
			    yaxis: {
			        tickSize: 500
			    }
			});

			$('#graph-bars').hide();
 
			$('#lines').on('click', function (e) {
			    $('#bars').removeClass('active');
			    $('#graph-bars').fadeOut();
			    $(this).addClass('active');
			    $('#graph-lines').fadeIn();
			    e.preventDefault();
			});
			 
			$('#bars').on('click', function (e) {
			    $('#lines').removeClass('active');
			    $('#graph-lines').fadeOut();
			    $(this).addClass('active');
			    $('#graph-bars').fadeIn().removeClass('hidden');
			    e.preventDefault();
			});

			function showTooltip(x, y, contents) {
			    $('<div id="tooltip">' + contents + '</div>').css({
			        top: y - 16,
			        left: x + 20
			    }).appendTo('body').fadeIn();
			}
			 
			var previousPoint = null;
			
			 
			$('#graph-lines, #graph-bars').bind('plothover', function (event, pos, item) {
			    if (item) {
			        if (previousPoint != item.dataIndex) {
			            previousPoint = item.dataIndex;
			            $('#tooltip').remove();
			            var x = item.datapoint[0],
			                y = item.datapoint[1];
			            var week = function () {
			                	if (x==1) {
			                		return "week";
			                	} else {
			                		return "weeks";
			                	};
			                };    
			                showTooltip(item.pageX, item.pageY, y + 'minutes of workdone after ' + x + week());
			        }
			    } else {
			        $('#tooltip').remove();
			        previousPoint = null;
			    }
			});
		});
